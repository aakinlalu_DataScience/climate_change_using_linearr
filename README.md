# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Using linear Regression to study the relationship between average global temperature and several other factors.

* Quick summary

* * There have been many studies documenting that the average global temperature   has been increasing over the last century. The consequences of a continued rise in global temperature will be dire. Rising sea levels and an increased frequency of extreme weather events will affect billions of people.

* * In this problem, we will attempt to study the relationship between average global temperature and several other factors.


### How do I get set up? ###
Using R
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact